package ru.vartanyan.tm.controller;
import ru.vartanyan.tm.api.service.IProjectTaskService;
import ru.vartanyan.tm.api.controller.ITaskController;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.enumerated.Sort;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.system.NullTaskException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    public TaskController(ITaskService taskService, IProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        System.out.println("[ENTER SORT:]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> list;
        if (sort == null || sort.isEmpty()) list = taskService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            list = taskService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Task task: list) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) throw new NullTaskException();
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByName() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskById() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        showTask(task);
        System.out.println("[OK]");
    }

    private void showTask(Task task) {
        System.out.println("[ID] " + task.getId());
        System.out.println("[NAME] " + task.getName());
        System.out.println("[DESCRIPTION] " + task.getDescription());
    }


    @Override
    public void removeTaskByIndex() throws Exception {
        System.out.println("[REMOVE TASK");
        System.out.println("[ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeOneByIndex(index);
        System.out.println("[TASK REMOVED]");
    }

    @Override
    public void removeTaskById() throws Exception {
        System.out.println("[REMOVE TASK");
        System.out.println("[ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        taskService.removeOneById(id);
        System.out.println("[TASK REMOVED]");
    }

    @Override
    public void removeTaskByName() throws Exception{
        System.out.println("[REMOVE TASK");
        System.out.println("[ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        System.out.println("[TASK REMOVED]");
    }

    @Override
    public void updateTaskByIndex() throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[INTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(index, name, description);
        System.out.println("[TASK UPDATED]");
    }

    @Override
    public void updateTaskById() throws Exception{
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER Id]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[INTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(id, name, description);
        System.out.println("[TASK UPDATED]");
    }

    @Override
    public void create() throws Exception{
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER MAME:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = taskService.add(name, description);
        System.out.println("[TASK CREATED]");
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void startTaskById() throws Exception {
        System.out.println("[START TASK]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(id);
        System.out.println("[TASK STARTED]");
    }

    @Override
    public void startTaskByName() throws Exception{
        System.out.println("[START TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(name);
        System.out.println("[TASK STARTED]");
    }

    @Override
    public void startTaskByIndex() throws Exception {
        System.out.println("[START TASK]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startTaskByIndex(index);
        System.out.println("[TASK STARTED]");
    }

    @Override
    public void finishTaskById() throws Exception{
        System.out.println("[FINISH TASK]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskById(id);
        System.out.println("[TASK FINISHED]");
    }

    @Override
    public void finishTaskByName() throws Exception{
        System.out.println("[START TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskByName(name);
        System.out.println("[TASK FINISHED]");
    }

    @Override
    public void finishTaskByIndex() throws Exception {
        System.out.println("[FINISH TASK]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.finishTaskByIndex(index);
        System.out.println("[TASK FINISHED]");
    }

    @Override
    public void updateTaskStatusById() throws Exception{
        System.out.println("[UPDATE TASK STATUS]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.updateTaskStatusById(id, status);
        System.out.println("[TASK STATUS UPDATED]");
    }

    @Override
    public void updateTaskStatusByName() throws Exception{
        System.out.println("[UPDATE TASK STATUS]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.updateTaskStatusByName(name, status);
        System.out.println("[TASK STATUS UPDATED]");
    }

    @Override
    public void updateTaskStatusByIndex() throws Exception {
        System.out.println("[UPDATE TASK STATUS]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.updateTaskStatusByIndex(index, status);
        System.out.println("[TASK STATUS UPDATED]");
    }

    @Override
    public void findAllByProjectId() throws Exception {
        System.out.println("[FIND ALL TASKS BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findAllTaskByProjectId(projectId);
        int index = 1;
        for (Task task: tasks){
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void bindTaskByProjectId() throws Exception {
        System.out.println("BIND TASK TO PROJECT BY PROJECT ID");
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskByProjectId(projectId, taskId);
        System.out.println("TASK ADDED TO PROJECT");
    }

    @Override
    public void unbindTaskByProjectId() throws Exception {
        System.out.println("UNBIND TASK TO PROJECT BY PROJECT ID");
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(projectId, taskId);
        System.out.println("TASK REMOVED FROM PROJECT");
    }

}
