package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
