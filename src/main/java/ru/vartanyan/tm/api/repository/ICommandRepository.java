package ru.vartanyan.tm.api.repository;

import ru.vartanyan.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
